#!/bin/bash

clear
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
echo "$SCRIPTPATH"
echo Creating virtual environment...
python3 -m venv "$SCRIPTPATH"/tmp/VirtEnv
echo Done.
echo Activating the virtual environment...
source "$SCRIPTPATH"/tmp/VirtEnv/bin/activate
echo Done.
echo Executing Python script...
python3 "$SCRIPTPATH"/project.py
#echo Deleting temporary files...
#rm -R "$SCRIPTPATH"/tmp
#echo Done.
