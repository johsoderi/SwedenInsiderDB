![example](https://me.qr.ax/sidb/jokkmokk.gif)

# SwedenInsiderDB

This is a project I started working on during the Python beginner course "Programming & Systems Design" at Nackademin in Stockholm 2018, but I've expanded on it since.
The intention was to provide an information platform for people inclined to relocate inside of Sweden, and for foreigners who plan to move here.
It aims to collect information about a place from different sources and present them in a neat way, giving the user a basic overview of a place they might be interested in.

To be completely honest though, it's probably more suited as a prop in an 80's action movie than anything else. But if you can stand the low-res graphics, you might find it interesting to just browse around the country! I've actually learned a whole lot while testing. Anyway, you can see it in action below (~~but please disregard the fact that two unrelated police reports shown in the video are about men masturbating in public. I'm pretty sure it's not <i>that</i> common!~~ You see? Not a single one in the new example vid!)

<b>Browsing around</b>
<br>
![](https://me.qr.ax/sidb/browsing3.mp4)

## Prerequisites

* The project is developed and tested under <b>macOS 11.4 & Python 3.9.2</b>. It runs pretty much as-is in Linux, but running it in Windows will more than likely need some tinkering.
* It uses a bunch of modules (weighing in at about <b>300MiB of disk space</b>), installed inside `SwedenInsiderDB/tmp/VirtEnv`
* You will need an <b>XTerm</b> compatible terminal emulator capable of showing <b>256 colors (8-bit)</b> or <b>"True Color" (24-bit)</b>, such as iTerm2 for macOS. Terminal.app won't show the colors correctly in the default 24-bit mode, but you can change mode by setting the variable `colorMode` in `SwedenInsiderDB/project.py`.
* A <b>MapBox API key</b>, which you can get for free on mapbox.com. The program runs without it, but you won't see the map.
* ~~Understanding of the <b>Swedish language</b>, if you want to read the police reports. Everything else is presented in English, though. If you know about a nice, free API for translating SE -> EN, please let me know!~~ The reports are now translated using the `googletrans` module (alpha version 3.1.0a0, since current release version 3.0.0 seems to be broken).

#### Recommended terminal settings:
* Terminal size: 118 * 62 (though unusually long police reports may require a taller window)
* Font: PT Mono Regular 12p (or any font that displays things correctly, you'll see what looks good)

## Known bugs
* If a police report is really long, it will f up the layout. I don't really want to truncate them though, just zoom out and hit c again!
* If one political party got close to 100% of the votes, that line would wrap in an ugly way. But that, like, doesn't happen (afaik).

## Usage

You can play around with a live version at https://www.anyfiddle.com/p/johsoderi/q9ebkopd

#### OR

Clone the repository:
```
$ git clone https://gitlab.com/johsoderi/SwedenInsiderDB.git
```
Enter the new directory:
```
$ cd SwedenInsiderDB
```
Create an account on https://www.mapbox.com and create an API key ("Access token"). Paste said key on line 15 of `SwedenInsiderDB/project.py`:
```
mapBoxApiKey = "MAPBOX-API-KEY-GOES-HERE"
```
Make `SwedenInsiderDB/run.sh` executable, then execute it:
```
$ chmod u+x run.sh
$ ./run.sh
```

This will create the dir `SwedenInsiderDB/tmp/VirtEnv/`, activate the environment and then run the main script, `SwedenInsiderDB/project.py`, which installs the necessary modules from `SwedenInsiderDB/misc/requirements.txt` inside the venv. ~~It will then combine geolocation data from geodata.txt (freely available, but outdated and inexact) with downloaded, user-submitted data from postnummeruppror.nu (more current and precise, but not yet complete) to build a geolocation "database" (or a pickled dictionary, to be exact). This will take a few minutes (as can be seen in the video below), but is only done once.~~

Since the demise of postnummeruppror.nu, the above no longer happens and a static db found inside the `data` directory will be used instead. This means that any land surveying data and postal code changes made after 2020-07-06 won't make it into the db. On the plus side, the first run no longer takes ages to execute!

If anyone is interested in the last file I downloaded from postnummeruppror, I made it available [here](https://me.qr.ax/sidb/samples-with-coordinates.osm.xml) (35.6MB).

<b>First run (downloading new location data, building db, etc):</b>
<br>
![](https://me.qr.ax/sidb/firstrun.mp4)

When all things are in place, the script will call the API's and present you with some basic stats on Stockholm, Sweden along with a crude map and the municipality's coat of arms.
You will also see a menu with the following options:
* #### [G]ENERAL
An extract of the municipality's Wikipedia entry. (Data source: https://sv.wikipedia.org/w/api.php)
* #### [P]OLITICS
An overview graph of the results from the latest Municipal Assembly elections. Bars for the 8 largest parties (on a national level) are color-coded based on their respective logos, smaller parties are aggregated into one white bar. (Data source: http://api.scb.se)
* #### [D]ATING
Let's you input the preferred sex(es) and age spans of your potential partners, and bluntly lays out your statistical chance of success (well, at least relative to other places) in the municipality. Pro-tip if you're into women over 95: In Älvkarleby, 7.1% are divorced and the rest of them are widows! (Data source: http://api.scb.se)
* #### [C]RIME
Prints the latest public police report from the area, now translated into English using the `googletrans` module. Also calculates the distance between the current location and the report coordinates. (Data source: https://brottsplatskartan.se/api)
* #### S[U]RVEY
Fetches parts of the "Satisfaction Index" from municipality citizen surveys, when it's available (some municipalities, like Stockholm, don't use the standard survey). (Data source: http://api.scb.se)
* #### [N/S/W/E]
Move around on the map.
* #### ZOOM [+/-]
Enhance! (or widen the perspective)
* #### [R]ANDOM
Shows a random place.
* #### [I]NPUT
Search for Town / Municipality / County / Postal code / Latitude + Longitude.
* #### E[X]IT
Quits the application.

## Built With

* [img_term](https://github.com/JonnoFTW/img_term) - Jonathan Mackenzie's 'img-term.py' converts the images to ANSI color codes. Thanks for sharing it, those ANSI graphics are just beautiful :)
#### For a list of dependencies, see /misc/requirements.txt.

### API:s
* [Brottsplatskartan](https://brottsplatskartan.se/) - Offers a free API for police reports.
* [MapBox: Static Images API](https://docs.mapbox.com/help/glossary/static-images-api/) - Spits out an image of a map (50.000 free requests per month!).
* [Wikipedia](https://en.wikipedia.org/) - Gives us an extract of municipality Wikipedia pages, coats of arms, area and population data.
* [SCB: Statistikdatabasen](http://www.statistikdatabasen.scb.se/) - A true goldmine of Swedish statistics! Used in the Dating, Politics and Citizen survey sections, as well as for annual income info.
* [Google Translate](https://cloud.google.com/translate) - Used by the googletrans module for translation of the latest police report.
* ~~[Postnummeruppror: Insamling](https://insamling.postnummeruppror.nu/nightly/samples-with-coordinates.osm.xml) - User submitted geo-data, because postal codes are not public information (for some strange reason).~~ The site seems to be have been down for a while, so the db will not update unless I implement another solution. A db file containing the last data is therefor included in the repo (`data/geodata.db`).

## Author

**johsoderi** - Graduated in 2020 from the DevOps Engineer program at Nackademin in Stockholm
