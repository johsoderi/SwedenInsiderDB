#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

# Course: Programmering & Systemering (Programming & Systems Design) - DevOps2018 @Nackademin, Stockholm, Sweden
# Project name: "SwedenInsiderDB"
# Author: johsoderi
# Recommended Terminal settings:
# Size: 118*61
# Font: PT Mono Regular 12p, Line spacing: 0,929
# Declare as XTerm-256color. Display ANSI colors.

#----/ SETTINGS /-----------------------------------------------------------------------------------------------#
colorMode = 24 # 8 (8-bit, 256 colors) or 24 (24-bit, True Color). Default: 24
debugLevel = 0 # 0: Debug off, 1: Critical exceptions, 2: All exceptions. See class "ContextManager". Default: 1
mapBoxApiKey = "MAPBOX-API-KEY-GOES-HERE" # Alternatively, create file apikey.txt and put api key as first line.
defaultZoomLevel = 2 # Possible values: 1 - 22. Default: 2
#---------------------------------------------------------------------------------------------------------------#


#####| BEGIN STYLING CODE |##################################################################
#  This is a template I made for ANSI escape code stuff.                                    #
#----/ Imports /-----------------------------------------------------------------------------
import os, platform
#----/ Color & Style /-----------------------------------------------------------------------
# For a great overview of ANSI escape sequences, see:
# https://stackoverflow.com/a/33206814/4329563
e = '\33['
fBlk, fRed, fGrn, fYel, fBlu, fMag, fCya, fWht = \
e+'30m',e+'31m',e+'32m',e+'33m',e+'34m',e+'35m',e+'36m',e+'37m'                  # Text color
bBlk, bRed, bGrn, bYel, bBlu, bMag, bCya, bWht = \
e+'40m',e+'41m',e+'42m',e+'43m',e+'44m',e+'45m',e+'46m',e+'47m'            # Background color
b, nb, i, ni, u, nu, bl, nbl = \
e+'1m',e+'22m', e+'3m', e+'23m', e+'4m',e+'24m', e+'5m', e+'25m'
alloff = e+'0m'                                                         # Reset color & style
# Project specific color codes:
bgc = 52 # Current section indicator color
dUp = f"\33[38;5;{bgc}m\33[48;2;9;97;22m"
lUp = f"\33[48;5;{bgc}m\33[38;2;9;97;22m"
da = f"\33[38;5;{bgc}m\33[48;5;{bgc}m"
l = f"\33[38;2;9;97;22m\33[48;2;9;97;22m"
od = f"\33[38;5;166;48;5;{bgc}m"
yd= f"\33[38;5;184;48;5;{bgc}m"
o = ForangeBdarkgray = e+"38;5;166;48;5;233m"
p = y = FyellowBdarkgray = e+"38;5;184;48;5;233m"
g = FgreenBdarkgray = e+"38;5;34;48;5;233m"
gr = FgrayBdarkgray = e+"38;5;249;48;5;233m"
r = FredBdarkgray = e+"38;5;160;48;5;233m"
gb = FgrayBblack = e+"38;5;238;48;5;232m"
rb = FredBblack = e+"38;5;124;48;5;232m"
#----/ Functions /---------------------------------------------------------------------------
def senBlirAlltSvart():                                                   # Clears the screen
    if (os.name == "posix"): os.system("clear")
    elif (platform.system() == "Windows"): os.system("cls")# I'll keep this in here, although
    else: pass                                             # this script is POSIX only.
def ln (newLines=1):                                # ln() prints 1 new line. ln(3) prints 3.
    print("\n" * newLines, end = "")
def mv(row, col):                                                          # Move text cursor
    stri = f"\33[{row};{col}H"
    print(stri, end="")
    return stri
def co (fg="", bg=""):                                               # More flexible coloring
    fg="184" if not fg else str(fg)
    bg="233" if not bg else str(bg)
    return "\33[38;5;"+fg+";48;5;"+bg+"m"
#####| END STYLING CODE |####################################################################


globPath = (os.path.dirname(__file__) + "/")

class VirtualEnvironment:
    def __init__(self):
        self.result = ""
        self.showOutput = ""
        #self.showOutput = "&> /dev/null" # To hide pip install output, uncomment this line.
    def install_dependencies(self):
        # The modules installed by this function were previously installed in a virtual environment,
        # and with the command 'pip freeze > ~/requirements.txt', I got the file containing all
        # dependencies, which is used here by 'pip install' to get them in the newly created virtualenv.
        print("\nInstalling necessary modules in the virtual environment")
        try:
            self.result = os.system("pip install -r \"" + globPath + "misc/requirements.txt\"" + self.showOutput)
            self.result = "Done.\n"
        except:
            self.result = "\nAn error occured during installation!\n" + self.result
            print(self.result)
            exit()

class ContextManager:
    '''
    This is my debugger/context manager. With this class you can simply add "with ease:" before any line
    you suspect to misbehave. You can choose to print all exceptions to the terminal, or to append them
    to a log file. An instance of this class can be called with "dangerLevel" (1: Critical, 2: Trivial)
    and a "contextComment". The "self.debugLevel" setting takes a value between 0-2:
    0: Debug off, 1: Critical exceptions, 2: All exceptions
    Examples:
    0:  ease = ContextManager()
    1:  with ease: x = 1 / 0
        if ease.ex:
            print("Gasp! How could this possibly go wrong!?")
    2:  with ease(1,"If this function ever breaks, call the president asap!"):
            NuclearTimeMachineCodeGenerator = 423542062354 ** "A 1989 Ford Fiesta"
    3:  with ease(2,"I know about the f*****g exception, capeesh? /Tony"):
            something = fuggedaboutit
    '''
    def __init__(self, dLev=1, comm="N/A"):
        self.debugLevel = debugLevel
        #self.log = "/path/to/log"
        self.log = None
        self.contextComment,self.dangerLevel,self.ct, self.ex = comm, dLev, 0, False
        self.levels = [f"\nSchrödingers Exception             \n",
                       f"\nPotentially Troublesome Exception  \n",
                       f"\nMostly Harmless Exception          \n"]
    def __call__(self,dangerLevel,contextComment):
        self.dangerLevel,self.contextComment = dangerLevel,contextComment
        return self
    def __enter__(self): self.ct += 1
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.ex = True if (exc_type != None) else False
        if (exc_type == None): self.ct -= 1
        if (exc_type != None) and (self.debugLevel >= self.dangerLevel):
            if self.log: l = open(self.log, "a+")
            fileName = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(f"\n  {'_'*31}{'_'*(len(str(self.ct)))}", file=l if self.log else stdout)
            print(f"_/ Context Manager - Exception #{self.ct} ",end='', file=l if self.log else stdout)
            print(f"\\{'_'*(46-(len(str(self.ct))))}",end='', file=l if self.log else stdout)
            print(f"\n{self.levels[self.dangerLevel]}", file=l if self.log else stdout)
            print(f"Type: \"{exc_type.__name__}\"\nValue: \"{exc_val}\"", file=l if self.log else stdout)
            print(f"Doc string: \"{exc_val.__doc__}\"", file=l if self.log else stdout)
            print(f"Context Comment: {self.contextComment}", file=l if self.log else stdout)
            print(f"File Name: {fileName}", file=l if self.log else stdout)
            print(f"Line Number {exc_tb.tb_lineno}:\n", file=l if self.log else stdout)
            with open(__file__,"r") as s:
                print(dedent(s.readlines()[exc_tb.tb_lineno-1].rstrip()), file=l if self.log else stdout)
            print(f"{'_'*80}\n", file=l if self.log else stdout)
            if self.log: l.close()
        return True

class Loader:
    def __init__(self, desc="Loading...", end="Done.", timeout=0.1):
        """
        I got this (slightly modified) class for a spinning loading char from ted@stackoverflow:
        https://stackoverflow.com/a/66558182/4329563
        """
        self.desc = desc
        self.end = self.desc + " √"
        self.timeout = timeout
        self._thread = Thread(target=self._animate, daemon=True)
        #self.steps = ["⢿", "⣻", "⣽", "⣾", "⣷", "⣯", "⣟", "⡿"]
        self.steps = ["|", "/", "-", "\\"]
        self.done = False
    def start(self):
        self._thread.start()
        return self
    def _animate(self):
        for c in cycle(self.steps):
            if self.done:
                break
            print(f"\r{self.desc} {c}", flush=False, end="")
            sleep(self.timeout)
    def __enter__(self):
        self.start()
    def stop(self):
        self.done = True
        cols = get_terminal_size((80, 20)).columns
        print("\r" + " " * cols, end="", flush=True)
        print(f"\r{self.end}", flush=True)
    def __exit__(self, exc_type, exc_value, tb):
        self.stop()



class GibData:
    def __init__(self):
        self.lat = 0.0
        self.long = 0.0
        senBlirAlltSvart()
        try:
            with Loader("Loading geolocation database"):
                with open(globPath + 'data/geodata.db', 'rb') as f:
                    self.database = pickle.load(f)
                # To make a human-readable file out of db:
                # with open(globPath + 'data/geodata.text', "a+") as f:
                #    for i in self.database:
                #        f.write(str(i) + "\n")
                #
                # Example for updating values in db:
                # for i in self.database:
                #     if i['municipality'] == "Heby":
                #         i['county'] = "Uppsala"
                #         i['muniCode'] = "0331"
                # with open(globPath + 'data/geodata.db', 'wb') as f:
                #     pickle.dump(self.database, f, 4) # 4: For compatibility with older python versions (3.4+)


        except:
            print("Geolocation database not found, building")
            self.upprorList = self.fetchUpprorData()
            self.database = []
            self.database = self.makeDatabase()
            with open(globPath + 'data/geodata.db', 'wb') as f:
                pickle.dump(self.database, f, pickle.HIGHEST_PROTOCOL)

    def distance(self, lat1, lon1, lat2, lon2):
        # I... did not come up with this code. Quite frankly I've forgotten where I found it.
        p = 0.017453292519943295
        a = 0.5 - cos((lat2-lat1)*p)/2 + cos(lat1*p)*cos(lat2*p) * (1-cos((lon2-lon1)*p)) / 2
        return 12742 * asin(sqrt(a))

    def gibClosest(self, v):
        return min(self.database, key=lambda p: self.distance(v['lat'],v['lon'],p['lat'],p['lon']))

    def gibRandom(self):
        placeData = []
        randPlace = randrange(1, len(self.database))
        place = self.database[randPlace]
        placeData = {
            'postalCode': place["postalCode"],
            'postalTown': place["postalTown"],
            'county': place["county"],
            'municipality': place["municipality"],
            'muniCode': place["muniCode"],
            'lat': float(place["lat"]),
            'lon': float(place["lon"])
        }
        return placeData

    def findByKey(self, kindOfStuff, value):
        placeData = []
        for place in self.database:
            if place[kindOfStuff].lower() == value.lower():
                placeData = {
                    'postalCode': place["postalCode"],
                    'postalTown': place["postalTown"],
                    'county': place["county"],
                    'municipality': place["municipality"],
                    'muniCode': place["muniCode"],
                    'lat': float(place["lat"]),
                    'lon': float(place["lon"])
                }
        return placeData

    def fetchUpprorData(self):
        geoUrl = "https://insamling.postnummeruppror.nu/nightly/samples-with-coordinates.osm.xml"
        geoPath = globPath + "tmp/pnu.xml"
        print("Checking geocoding data: "+globPath+"tmp/pnu.xml")
        dlGeo = False
        try:
            geoModDateStr = check_output("stat -t %c " + geoPath + " 2>/dev/null | cut -f4 -d'\"'", shell=True).decode().rstrip()
            geoModDate = datetime.strptime(geoModDateStr, '%c')
            geoFileAge = (datetime.now() - geoModDate).days
            if geoFileAge > 7:
                print("Geocoding data is " + str(geoFileAge) + " day(s) old. Starting download")
                dlGeo = True
        except:
            print("Geocoding data not found. Starting download")
            dlGeo = True
        if dlGeo:
            start = datetime.now()
            os.system("(wget -O " + geoPath + " " + geoUrl + " &> "+globPath+"tmp/processoutput &) &> /dev/null")
            # Should find a cleaner way for the above, but haven't yet managed to pipe realtime wget output to a var w/o printing to screen...
            sleep(0.5)
            geoSize = ""
            while geoSize == "":
                with open(globPath+"tmp/processoutput", "r") as p:
                    lines = p.readlines()
                try:
                    geoSize = lines[4].split("(")[1].split(")")[0]
                except:
                    mv(5,0)
                    print("Downloading geocoding data [0K/??M] Time elapsed: "+str(datetime.now()-start)[0:7]+"\n"+geoUrl+" --> "+"./tmp/pnu.xml")
                    sleep(0.5)
            current = ""
            processAlive = 1
            while processAlive > 0:
                with open(globPath+"tmp/processoutput", "r") as p:
                    lines = p.readlines()
                try:
                    progressKb = lines[-1].split(".")[0].replace(" ", "")
                except:
                    pass
                progress = progressKb+"/"+geoSize if progressKb else "0K/??M"
                mv(5,0)
                print("Downloading geocoding data ["+progress+"] Time elapsed: "+str(datetime.now()-start)[0:7]+"\n"+geoUrl+" --> "+"./tmp/pnu.xml")
                sleep(0.1)
                processAlive = int(check_output("ps -e | grep [w]get | wc -l", shell=True).decode())
            os.system("rm "+globPath+"tmp/processoutput")
        else:
            print("Geocoding data up to date!")
        return self.processUpprorData()

    def processUpprorData(self):
        self.upprorList = []
        tree = ET.parse(globPath+'tmp/pnu.xml')
        root = tree.getroot()
        upprorSize = len(root)
        i = 0
        start = datetime.now()
        for elem in root:
            i += 1
            mv(7,0)
            print("Processing geocoding data from postnummeruppror.nu ["+str(i)+"/"+str(upprorSize)+"] Time elapsed: "+str(datetime.now() - start)[0:7])
            lineDict = {'lat': float(elem.attrib['lat']), 'lon': float(elem.attrib['lon'])}
            for part in elem:
                lineDict[part.attrib['k']] = part.attrib['v']
            if ("lat" in lineDict) & ("lon" in lineDict) & ("addr:postcode" in lineDict):
                if len(lineDict["addr:postcode"]) == 5:
                    try:
                        lineDict = {
                            "postalCode": lineDict["addr:postcode"],
                            "postalTown": lineDict["addr:city"].title(),
                            "lat": lineDict["lat"], "lon": lineDict["lon"]
                        }
                    except:
                        lineDict = {
                            "postalCode": lineDict["addr:postcode"],
                            "postalTown": "", "lat": lineDict["lat"],
                            "lon": lineDict["lon"]
                        }
                    self.upprorList.append(lineDict)
        return self.upprorList

    def searchUppror(self, postalCode):
        postalCode = postalCode.replace(" ", "")
        upprorData = []
        for place in self.upprorList:
            if place["postalCode"] == postalCode:
                upprorData = {
                    'postalCode': place['postalCode'],
                    'postalTown': place['postalTown'],
                    'lat': float(place['lat']),
                    'lon': float(place['lon']),
                }
        return upprorData

    def makeDatabase(self):
        path = globPath+"data/geodata.txt"
        with open(path, "r") as datafile:
            size = str(sum(1 for line in datafile))
            datafile.seek(0)
            i = 0
            start = datetime.now()
            for line in datafile:
                i += 1
                mv(8,0)
                print("Building geocoding database ["+str(i)+"/"+size+"] Time elapsed: "+str(datetime.now() - start)[0:7])
                theLine = line.split("\t")
                muniCode = theLine[6]
                if len(muniCode) == 3: # The municipality codes in geodata.txt are missing the leading zeroes.
                    muniCode = ("0" + str(muniCode))
                postalCode = theLine[1].replace(" ", "")
                lineDict = {
                    'postalCode': postalCode,
                    'postalTown': theLine[2],
                    'county': theLine[3],
                    'municipality': theLine[5],
                    'muniCode': muniCode,
                    'lat': float(theLine[9]),
                    'lon': float(theLine[10])}
                upprorData = self.searchUppror(lineDict['postalCode'])
                if len(upprorData) != 0:
                    if len(upprorData['postalTown']) > 0:
                        lineDict['postalTown'] = upprorData['postalTown']
                    lineDict['lat'] = upprorData['lat']
                    lineDict['lon'] = upprorData['lon']
                self.database.append(lineDict)
        return self.database

class Image:
    global placeData
    # This class uses img_term.py by Jonathan Mackenzie.
    # It converts images into ANSI color codes.
    # https://github.com/JonnoFTW/img_term
    def __init__(self, name, width):
        self.colorBits = str(colorMode)
        self.name = name
        self.width = str(width)
        self.result = ""
        self.convert()
    def convert(self):
        # This function tells img_term.py to make a terminal friendly version of an image, in
        # 8-bit or 24-bit color. I could only get 24-bit to display properly in iTerm2 and not in MacOS Terminal.app, so choose what works.
        with ease(2, "Error converting image. Invalid API key perhaps?"):
            self.result = os.system("python3 " + globPath + "misc/" + "img_term.py -img " + globPath\
            + self.name + " -width " + self.width + " -col "+self.colorBits+" > " + globPath + self.name + ".txt 2>/dev/null")
            if int(self.result) > 0: raise Exception("img_term.py exit code: " + str(self.result))
            # I know, I know. But we don't get any error specifics from the script, and I want to keep it as-is for easy updates.
        # Since img_term.py puts some screen clearing and positioning ANSI codes in the beginning
        # and end of the file, and I don't want to change the original script, the bytes in question
        # are cut off with these lines:
        with open(globPath + self.name + ".txt", "rb") as f:
            bytes_read = f.read()
        cut_bytes = bytes_read[9:-5]
        with open(globPath + self.name + ".txt", "wb") as f:
            f.write(cut_bytes) # Here the new version is saved to file.
        return self.result
    def display(self, xloc, yloc, cropL, cropR, cropT, cropB):
        # In and by itself, img_term just clears the screen and puts the image in the top-left corner.
        # I wanted to be able to put it where I want and I also needed to be able to crop it, so this
        # function deals with those things.
        self.xloc = str(xloc)
        self.yloc = str(yloc)
        self.cropL = cropL
        self.cropR = cropR
        self.cropT = cropT
        self.cropB = cropB
        with open(globPath + self.name + ".txt", "r") as f:
            lines = [line.rstrip() for line in f]
        for x in range(self.cropT,len(lines)-self.cropB):
            lines[x] = lines[x].split("▀") # The char that "images" are made of, I use it to count pixels.
            if self.cropR > 0: # It wouldn't take "[:-0]", so I had to make two versions of the crop stuff.
                lines[x] = "▀".join(lines[x][self.cropL:-self.cropR]) # One if right side cropping is used,
            else: # and one if we don't crop at all, or just the left side:
                lines[x] = "▀".join(lines[x][self.cropL:]) # To position the image, I use "\33[Y;XH":
            print(f"\33[{x-self.cropT + int(self.yloc) + 1};{int(self.xloc) + 1}H", end="")
            print(lines[x])
        print('\33[0m')

class Draw:
    def drawFrame(self):
        global placeData
        global zoomLevel
        global menuChoice
        senBlirAlltSvart() # This just clears the screen, as defined in the styling template at the beginning of the script.
        print("\33[38;48;5;233m" + "          "*2000) # An ugly way of painting the screen in the color I want.
        mv(0,0) # Another styling function, used as a cleaner way to print positional ANSI codes.
        self.displayCursive(placeData['postalTown'])
        mapImg.display(xloc=26, yloc=10, cropL=30, cropR=30, cropT=10, cropB=10)
        coatImg.display(xloc=85, yloc=10, cropL=0, cropR=0, cropT=0, cropB=0)
        frameImg.display(xloc=0, yloc=10, cropL=0, cropR=0, cropT=0, cropB=0)
        menuImg.display(xloc=0, yloc=30, cropL=0, cropR=0, cropT=0, cropB=0)
        d = placeData
        z = zoomLevel
        print(f"{p}{b}")
        print(f"{mv(29,27)}{wiki.catchPhrase}")
        print(f"{mv(12,3)}LAT {d['lat']}")
        print(f"{mv(13,3)}LON {d['lon']}")
        print(f"{mv(14,3)}ZOOM {z}/22")
        print(f"{mv(15,3)}POST# {d['postalCode']}")
        print(f"{mv(16,3)}TOWN {d['postalTown']}")
        print(f"{mv(17,3)}MUNI {d['municipality']}")
        print(f"{mv(18,3)}CNTY {d['county']}")
        print(f"{mv(20,3)}MUNICIPALITY:")
        print(f"{mv(21,3)}CODE {placeData['muniCode']}")
        print(f"{mv(22,3)}AREA {wiki.area} Km2")
        print(f"{mv(23,3)}POPULATION {wiki.population}")
        print(f"{mv(24,3)}DENSITY {wiki.popDensity} Ppl/Km2")
        print(f"{mv(25,3)}ANN INCOME (AGE20-64)")
        print(f"{mv(26,3)}MEAN SEK{economy.salaryMean}K")
        print(f"{mv(27,3)}MEDIAN SEK{economy.salaryMedian}K")
        if menu.section.lower() == "g": self.highlightSection(f"{od}G{yd}ENERAL{da}▀▀", "0")
        if menu.section.lower() == "p": self.highlightSection(f"{od}P{yd}OLITICS{da}▀", "12")
        if menu.section.lower() == "d": self.highlightSection(f"{od}D{yd}ATING{da}▀▀▀", "23")
        if menu.section.lower() == "c": self.highlightSection(f"{od}C{yd}RIME{da}▀▀▀▀", "34")
        if menu.section.lower() == "u": self.highlightSection(f"{yd}S{od}U{yd}RVEY{da}▀▀▀", "45")
        if menu.section.lower() == "r": self.highlightSection(f"{od}R{yd}ANDOM{da}▀▀▀", "78")
        if menu.section.lower() == "i": self.highlightSection(f"{od}I{yd}NPUT{da}▀▀▀▀", "89")

    def highlightSection (self, name, hLoc):
        btnTop = f"{mv(31,hLoc)}{l}▀{lUp}"+"▀"*10+f"{l}▀{y}{mv(32,hLoc)}{l}▀{da}▀"
        btnBtm = f"{l}▀{y}{mv(33,hLoc)}{l}▀{dUp}"+"▀"*10+f"{l}▀{y}"
        print(btnTop+name+btnBtm)

    def displayCursive(self, text):
        # This method shows the name of the municipality in large, friendly letters on the top of the screen. I made the
        # "font" using the same script, img_term.py, that I use for converting the other graphics. I spent so much time
        # manually changing small things here and there though, so I wrote a script for automating the conversion process.
        # It's in the folder "FontMaker", and takes individual letters in png, named after the char they represent, and
        # and the width in pixels (e.g. "A13.png") and converts them into a font that can be displayed by this function.
        X = [""]*len(text)
        wordLine = [""]*10
        for letter in range(len(text)):
            value = hex(ord(text[letter]))
            with open(globPath + "data/" + "font_txt/" + str(value) + ".txt", "r") as f:
                X[letter] = f.readlines()
        for line in range(10):
            for letter in range(len(text)):
                wordLine[line] += X[letter][line].rstrip()
        for i in range(len(wordLine)):
            print(wordLine[i], end='\n')
        print(len(wordLine[0]))
        return

class Menu:
    global zoomLevel
    global placeData
    global newLoc
    def __init__(self):
        self.menuChoice = ""
        self.section = ""

    def getch(self):
        fd = stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            setraw(fd)
            ch = stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

    def hasMoved(self, newLoc):
        # Always update the map, but the rest only if postcode has changed!
        global placeData
        term = {'lat':float(placeData['lat']), 'lon':float(placeData['lon'])}
        closest = bigData.gibClosest(newLoc)
        if closest['postalCode'] != placeData['postalCode']:
            placeData = closest
            map.update()
            mv(36,0)
            wiki.update()
            economy.update()
            mapImg = Image(name="tmp/map.jpg", width=118)
            coatImg = Image(name="tmp/coat.png", width=32)
            draw.drawFrame()
            mv(35,0)
            print(fill(wiki.text, 110), y)
            self.section = "g"
            menu.menu()
        else:
            placeData['lat'] = float(newLoc['lat'])
            placeData['lon'] = float(newLoc['lon'])
            map.update()
            mapImg = Image(name="tmp/map.jpg", width=118)
            draw.drawFrame()
            mv(35,0)
            print("Nothing new here", y)
        print(f"{y}")
        self.menu()

    def menu(self):
        global zoomLevel
        global placeData
        global newLoc
        zoomStep = 1
        moveStep = 1 / (zoomLevel*2)
        print(f"{alloff}{b}")
        mv(32,3)
        print(f"{o}G{y}ENERAL")
        mv(32,14)
        print(f"{o}P{y}OLITICS")
        mv(32,25)
        print(f"{o}D{y}ATING")
        mv(32,36)
        print(f"{o}C{y}RIME")
        mv(32,47)
        print(f"{y}S{o}U{y}RVEY")
        mv(32,58)
        print(f"{o}N{y}/{o}S{y}/{o}W{y}/{o}E")
        mv(32,69)
        print(f"{y}ZOOM {o}-{y}/{o}+")
        mv(32,80)
        print(f"{o}R{y}ANDOM")
        mv(32,91)
        print(f"{o}I{y}NPUT")
        mv(32,102)
        print(f"{y}E{o}X{y}IT")
        if self.section.lower() == "g": draw.highlightSection(f"{od}G{yd}ENERAL{da}▀▀", "0")
        if self.section.lower() == "p": draw.highlightSection(f"{od}P{yd}OLITICS{da}▀", "12")
        if self.section.lower() == "d": draw.highlightSection(f"{od}D{yd}ATING{da}▀▀▀", "23")
        if self.section.lower() == "c": draw.highlightSection(f"{od}C{yd}RIME{da}▀▀▀▀", "34")
        if self.section.lower() == "u": draw.highlightSection(f"{yd}S{od}U{yd}RVEY{da}▀▀▀", "45")
        if self.section.lower() == "i": draw.highlightSection(f"{od}I{yd}NPUT{da}▀▀▀▀", "89")
        mv(60,0)
        print(" ")
        self.menuChoice = self.getch()
        if self.menuChoice != "":
            self.section = self.menuChoice

        # GENERAL
        if self.menuChoice == "G" or self.menuChoice == "g":
            draw.drawFrame()
            mv(35,0)
            wiki.update()
            for i in range(35,60):
                mv(i,0)
                print(" " * 110)
            mv(35,0)
            print(fill(wiki.text, 110))
            print(f"{y}")
            self.menu()

        # DATING
        if self.menuChoice == "D" or self.menuChoice == "d":
            draw.drawFrame()
            dating.date()
            mv(35,0)
            print(f"{y}")
            self.menu()

        # CRIME
        if self.menuChoice == "C" or self.menuChoice == "c":
            draw.drawFrame()
            mv(35,0)
            latestCrime.update()
            mv(35,0)
            print(latestCrime.text)
            print(f"{y}")
            self.menu()

        # CITIZEN SURVEY
        if self.menuChoice == "U" or self.menuChoice == "u":
            draw.drawFrame()
            mv(35,0)
            citizenSurvey.update()
            mv(35,0)
            print(citizenSurvey.text)
            print(f"{y}")
            self.menu()

        # POLITICS
        if self.menuChoice == "P" or self.menuChoice == "p":
            draw.drawFrame()
            mv(35,0)
            polls = politics.getPartyList()
            mv(35,0)
            print(polls)
            print(f"{y}")
            self.menu()

        # ZOOM +/-
        if self.menuChoice == "+":
            if zoomLevel < 22:
                draw.drawFrame()
                print(mv(35,0) + "Enhancing")
                zoomLevel += zoomStep
                map.update()
                mapImg = Image(name="tmp/map.jpg", width=118)
                draw.drawFrame()
            mv(35,0)
            print(f"{y}")
            self.menu()
        if self.menuChoice == "-":
            if zoomLevel > 1:
                draw.drawFrame()
                print(mv(35,0) + "Widening perspective")
                zoomLevel -= zoomStep
                map.update()
                mapImg = Image(name="tmp/map.jpg", width=118)
                draw.drawFrame()
            mv(35,0)
            print(f"{y}")
            self.menu()

        # N/S/W/E
        if self.menuChoice == "N" or self.menuChoice == "n":
            draw.drawFrame()
            print(mv(35,0) + "Going North")
            newLoc = {'lat':float(placeData['lat'] + (moveStep/3)), 'lon':float(placeData['lon'])}
            self.hasMoved(newLoc)
        if self.menuChoice == "S" or self.menuChoice == "s":
            draw.drawFrame()
            print(mv(35,0) + "Going South")
            newLoc = {'lat':float(placeData['lat'] - (moveStep/3)), 'lon':float(placeData['lon'])}
            self.hasMoved(newLoc)
        if self.menuChoice == "W" or self.menuChoice == "w":
            draw.drawFrame()
            print(mv(35,0) + "Going West")
            newLoc = {'lat':float(placeData['lat']), 'lon':float(placeData['lon'] - (moveStep/2))}
            self.hasMoved(newLoc)
        if self.menuChoice == "E" or self.menuChoice == "e":
            draw.drawFrame()
            print(mv(35,0) + "Going East")
            newLoc = {'lat':float(placeData['lat']), 'lon':float(placeData['lon'] + (moveStep/2))}
            self.hasMoved(newLoc)

        # INPUT (SEARCH)
        if self.menuChoice == "I" or self.menuChoice == "i":
            draw.drawFrame()
            mv(35,0)
            search.type()
            print(f"{y}")
            self.menu()

        # RANDOM PLACE
        if self.menuChoice == "R" or self.menuChoice == "r":
            zoomLevel = defaultZoomLevel
            draw.drawFrame()
            self.section = "g"
            mv(35,0)
            with Loader("Looking for a random place"):
                placeData = bigData.gibRandom()
            print(y+"Found "+o+placeData['postalTown']+", "+placeData['municipality']+", "+placeData['county']+y)
            #print("Getting map data")
            map.update()
            wiki.update()
            #print("Getting income data")
            #economy.update()
            with Loader("Converting images"):
                mapImg = Image(name="tmp/map.jpg", width=118)
                coatImg = Image(name="tmp/coat.png", width=32)
            draw.drawFrame()
            mv(35,0)
            print(fill(wiki.text, 110), y)
            menu.menu()

        # EXIT
        if self.menuChoice == "X" or self.menuChoice == "x":
            senBlirAlltSvart()
            mv(35,0)
            print(f"{y}Quitting application", end="\33[0m\n\n")
            exit()
        else:
            mv(57,0)
            self.menu()

class GeneralApi:
    # The only thing this class does is to take a url and desired file name, fetch the Info,
    # and save it as byte data to a file if a filename is given. If not, it returns that data
    # to the class that requested it.
    def __init__(self):
        pass
    def getStuff(self, url, fileName):
        self.headers = {"User-Agent": "SwedenInsiderDB/0.3 (https://gitlab.com/johsoderi/SwedenInsiderDB; j@qr.ax) Requests/2.26.0"}
        self.request = Session()
        self.data = self.request.get(url, headers=self.headers)
        if fileName != "":
            with open(globPath + fileName, "wb") as self.f:
                self.f.write(self.data.content)
        else:
            return self.data.content

class Maps(GeneralApi):
    global zoomLevel
    global placeData
    global mapBoxApiKey
    def __init__(self, width, height, fileName):
        self.width = width
        self.height = height
        self.mapFileName = fileName
        self.indicator = "https%3A%2F%2Fqr.ax%2Fm537"
        with ease(2, "apikey.txt not found, using local variable."):
            with open(globPath+"apikey.txt", "r") as apikeyfile:
                self.mapBoxApiKey = apikeyfile.readlines()[0].rstrip()
        if ease.ex:
            self.mapBoxApiKey = mapBoxApiKey
        self.update()
    def update(self):
        with Loader("Getting map"):
            self.urlList = [
                "https://api.mapbox.com/styles/v1/mapbox/satellite-v9/static/url-",
                self.indicator, "(",
                str(placeData['lon']), ",",
                str(placeData['lat']), ")/",
                str(placeData['lon']), ",",
                str(placeData['lat']), ",",
                str(zoomLevel), "/",
                str(self.width), "x",
                str(self.height),
                "?access_token=",
                self.mapBoxApiKey,
                "&logo=false"
            ]
            url = "".join(self.urlList)
            self.getStuff(url, self.mapFileName)

class Crimes(GeneralApi):
    # This class takes the current coordinates to get the latest police report from the area.
    # The response html code is a bit mangled by Google Translate, hence the mess below.
    def __init__(self):
        self.update()
    def update(self):
        with Loader("Getting police reports"):
            self.urlList = ["https://brottsplatskartan.se/api/eventsNearby?lat=", str(placeData['lat']), "&lng=", str(placeData['lon'])]
            self.url = "".join(self.urlList)
            self.fileName = ""
            with ease(2, "brottsplatskartan.se seems to be down."):
                self.crimeJSON = self.getStuff(self.url, self.fileName)
                json_response = loads(self.crimeJSON)
            if ease.ex:
                pass #?
            with ease(2, "Exeption thrown if no police report is found"):
                self.location = json_response["data"][0]["title_location"]
                self.lat = json_response["data"][0]["lat"]
                self.lon = json_response["data"][0]["lng"]
                self.distance = round(float(bigData.distance(self.lat, self.lon, placeData['lat'], placeData['lon'])))
                self.sweTitle = json_response["data"][0]["description"]
                self.engTitle = r + translator.translate(self.sweTitle, src='sv', dest='en').text + nb + gr
                self.sweContent = json_response["data"][0]["content"]
                self.engContent = translator.translate(self.sweContent, src='sv', dest='en').text
                self.engContent = self.engContent.replace("<br>", "\n")
                self.engContent = self.engContent.replace(" <br /> ", "\n")
                self.engContent = self.engContent.replace(" <br />", "\n")
                self.engContent = self.engContent.replace("<br /> ", "\n")
                self.engContent = self.engContent.replace("<p> ", "\n")
                self.engContent = self.engContent.replace(" </p>", "")
                self.engContent = self.engContent.replace(" <strong> ", b)
                self.engContent = self.engContent.replace("<strong> ", b)
                self.engContent = self.engContent.replace(" </strong> ", nb + ' ')
                self.engContent = self.engContent.replace(" </strong>", nb + ' ')
                self.engContent = self.engContent.replace("</strong> ", nb + ' ')
                self.time = "\33[33m" + json_response["data"][0]["pubdate_iso8601"][:10] + " " + \
                            json_response["data"][0]["pubdate_iso8601"][11:16] + p
            if ease.ex:
                self.engTitle = "The police in " + placeData['municipality'] + " haven't reported anything lately!"
                self.engContent = ""
                self.time = "N/A"
                self.location = "N/A"
                self.distance = "N/A"
            self.engLines = self.engContent.splitlines()
            self.text = p + "Latest police report from the area: " + self.time + " in " + self.location + " (" + str(self.distance) + "Km from current location)\n\n"
            self.text += self.engTitle + "\n"
            for line in self.engLines:
                self.text += fill(line, 117, replace_whitespace=False) + "\n"

class Wikipedia(GeneralApi):
    def __init__(self):
        self.update()
    def update(self):
        # So far, I've only found one municipality pretentious enough to anglicize the title of their Wikipedia
        # page, causing all API calls to English Wikipedia using the Swedish name to return garbage. While this
        # seems entirely appropriate to me, it is the second largest city in the country and excluding it from
        # this application might cause some fish-smelling dork named Glenn to choke on his herring just as he's
        # reaching the punchline of some hilarious dad-joke. We wouldn't want that to happen, so the next line
        # of code is dedicated to you, Glenn. You're welcome.
        municipality = "Gothenburg" if placeData['municipality'] == "Göteborg" else placeData['municipality']
        # And now Gotland suddenly doesn't want to be a municipality anymore, but instead a "region"...
        if placeData['municipality'] == "Gotland": municipality = "Region" 
        muniOrGotland = "_Municipality" if placeData['municipality'] != "Gotland" else "_Gotland"
        with Loader("Getting Wikipedia data"):
            with ease:
                fileName = ""
                global ext
                ext = ""
                notFound = True
                while notFound:
                    url = "https://en.wikipedia.org/w/api.php?action=query&format=json&prop=extracts&titles="\
                    + municipality + muniOrGotland + ext + "&exchars=1000&exintro=1&explaintext=1&exsectionformat=plain"
                    wikiText = self.getStuff(url, fileName)
                    if b"Municipality may refer to" not in wikiText:
                        notFound = False
                    else:
                        ext = ",_Sweden"
                        sleep(0.2)
                wikiTextDict = loads(wikiText)
            with ease:
                self.text = list(wikiTextDict["query"]["pages"].values())[0]["extract"]
            if ease.ex: self.text = "N/A"
            wikiEntityUrl = "https://en.wikipedia.org/w/api.php?action=query&prop=pageprops&titles="\
                            + municipality + muniOrGotland + ext + "&format=json"
            wikiEntityJson = self.getStuff(wikiEntityUrl, fileName)
            wikiEntityDict = loads(wikiEntityJson)
            with ease:
                wikiEntity = list(wikiEntityDict["query"]["pages"].values())[0]["pageprops"]["wikibase_item"]
                wikiDataUrl = "https://www.wikidata.org/wiki/Special:EntityData/" + wikiEntity + ".json"
                wikiDataJson = self.getStuff(wikiDataUrl, fileName)
                wikiDataDict = loads(wikiDataJson)
        self.dialCode = "N/A"
        #with Loader("Getting local dial code"):
        #    with ease:
        #        dialCode = wikiDataDict["entities"][wikiEntity]["claims"]["P473"][-1]["mainsnak"]["datavalue"]["value"]
        #    if ease.ex: self.dialCode = "N/A"
        # Turned out P473 isn't used for many muni's... Bummer, might hard-code a dict or fetch area codes from somewhere else.
        with Loader("Getting population data"):
            with ease:
                population = wikiDataDict["entities"][wikiEntity]["claims"]["P1082"][-1]["mainsnak"]["datavalue"]["value"]["amount"]
                self.population = round(float(population[1:]))
            if ease.ex: self.population = "N/A"
        with Loader("Getting area data"):
            with ease:
                area = wikiDataDict["entities"][wikiEntity]["claims"]["P2046"][0]["mainsnak"]["datavalue"]["value"]["amount"]
                self.area = round(float(area[1:]))
            if ease.ex: self.area = "N/A"
        with Loader("Calculating population density"):
            with ease: 
                self.popDensity = round(self.population / self.area)
            if ease.ex: self.popDensity = "N/A"
        with Loader("Getting catchphrase"):
            with ease:
                self.catchPhrase = ""
                catchPhraseDict = wikiDataDict["entities"][wikiEntity]["claims"]["P6251"]
                for i in range(0,len(catchPhraseDict)):
                    if (catchPhraseDict[i]["mainsnak"]["datavalue"]["value"]["language"] == "en"):
                        self.catchPhrase = "\"" + catchPhraseDict[i]["mainsnak"]["datavalue"]["value"]["text"] + "\""
                        break
                    else:
                        sweCatchPhrase = "\"" + catchPhraseDict[i]["mainsnak"]["datavalue"]["value"]["text"] + "\""
                        engCatchPhrase = translator.translate(sweCatchPhrase, src='sv', dest='en').text
                        self.catchPhrase = engCatchPhrase + " (tr.)"
                if self.catchPhrase == "":
                    self.catchPhrase = "\"" + catchPhraseDict[0]["mainsnak"]["datavalue"]["value"]["text"] + "\""
                self.catchPhrase = placeData['municipality'] + ": " + self.catchPhrase
            if ease.ex: self.catchPhrase = placeData['municipality'] + " is lame, they don't have a catchphrase."
        self.catchPhrase = fill(self.catchPhrase, width=88)
        with Loader("Getting coat of arms"):
            with ease:
                coatImgName = wikiDataDict["entities"][wikiEntity]["claims"]["P94"][0]["mainsnak"]["datavalue"]["value"]
                coatImgGetUrlPropUrl = "https://en.wikipedia.org/w/api.php?action=query&titles=Image:"\
                                        + coatImgName + "&prop=imageinfo&iiprop=url&format=json"
                coatImgUrlPropJson = self.getStuff(coatImgGetUrlPropUrl, fileName)
                coatImgUrlPropDict = loads(coatImgUrlPropJson)
                coatImgSvgUrl = list(coatImgUrlPropDict["query"]["pages"].values())[0]["imageinfo"][0]["url"]
                coatImgUrl = coatImgSvgUrl.replace("commons", "commons/thumb") + "/124px-" + coatImgName + ".png"
            if ease.ex: coatImgUrl = "https://qr.ax/m537"
            self.getStuff(coatImgUrl, "tmp/coat.png")

class Economy:
    def __init__(self):
        self.update()
    def update(self):
        with Loader("Getting income data"):
            self.salaryMedian = self.getSalaryMedian()
            self.salaryMean = self.getSalaryMean()
    def getSalaryMedian(self):
        medianPayload = {"query":[{"code":"Region","selection":{"filter":"vs:RegionKommun07EjAggr","values":[placeData['muniCode']]}}, \
                        {"code":"Kon","selection":{"filter":"item","values":["1+2"]}}, \
                        {"code":"Alder","selection":{"filter":"vs:Ålder4gr","values":["20-64"]}}, \
                        {"code":"ContentsCode","selection":{"filter":"item","values":["000001ON"]}}, \
                        {"code":"Tid","selection":{"filter":"item","values":["2018"]}}],"response":{"format":"json"}}
        with ease(2, "API response error"):
            resp = post("http://api.scb.se/OV0104/v1/doris/sv/ssd/START/HE/HE0110/HE0110A/NetInk02",data=dumps(medianPayload))
            # The response data is preceded with a BOM signature, so it can't be used as a dict directly (we have to shave it off first)
            data = resp.text.encode().decode("utf-8-sig")
        with ease(2, "Error evaluating income data"):
            data = eval(data)
        with ease(2, "Error parsing income data"):
            salaryMedian = float(data["data"][0]["values"][0])
        if ease.ex:
            salaryMedian = 0
        return salaryMedian
    def getSalaryMean(self):
        meanPayload = {"query":[{"code":"Region","selection":{"filter":"vs:RegionKommun07EjAggr","values":[placeData['muniCode']]}}, \
                        {"code":"Kon","selection":{"filter":"item","values":["1+2"]}}, \
                        {"code":"Alder","selection":{"filter":"vs:Ålder4gr","values":["20-64"]}}, \
                        {"code":"ContentsCode","selection":{"filter":"item","values":["000001OR"]}}, \
                        {"code":"Tid","selection":{"filter":"item","values":["2018"]}}],"response":{"format":"json"}}
        with ease(2, "API response error"):
            resp = post("http://api.scb.se/OV0104/v1/doris/sv/ssd/START/HE/HE0110/HE0110A/NetInk02",data=dumps(meanPayload))
            # The response data is preceded with a BOM signature, so it can't be used as a dict directly (we have to shave it off first)
            data = resp.text.encode().decode("utf-8-sig")
        with ease(2, "Error evaluating income data"):
            data = eval(data)
        with ease(2, "Error parsing income data"):
            salaryMean = float(data["data"][0]["values"][0])
        if ease.ex:
            salaryMean = 0
        return salaryMean

class Politics:
    def __init__(self):
        self.text = self.getPartyList()
    def getPartyList(self):
        with Loader("Getting election results"):
            payload = {"query":[{"code":"Region","selection":{"filter":"vs:RegionKommun07+BaraEjAggr","values":[placeData['muniCode']]}},\
                      {"code":"ContentsCode","selection":{"filter":"item","values":["ME0104B2"]}},{"code":"Tid",\
                       "selection":{"filter": "item","values":["2018"]}}],"response":{"format":"json"}}
            with ease(2, "API response error"):
                resp = post("http://api.scb.se/OV0104/v1/doris/sv/ssd/START/ME/ME0104/ME0104A/ME0104T1",data=dumps(payload))
                # The response data is preceded with a BOM signature, so it can't be used as a dict directly (we have to shave it off first)
                data = resp.text.encode().decode("utf-8-sig")
            with ease(2, "Error evaluating election data"):
                data = eval(data)
            with ease(2, "Error parsing election data"):
                parties = {}
                parties["Vänsterpartiet"] = float(data["data"][6]["values"][0])
                parties["Socialdemokraterna"] = float(data["data"][5]["values"][0])
                parties["Miljöpartiet"] = float(data["data"][4]["values"][0])
                parties["Centerpartiet"] = float(data["data"][1]["values"][0])
                parties["Kristdemokraterna"] = float(data["data"][3]["values"][0])
                parties["Liberalerna"] = float(data["data"][2]["values"][0])
                parties["Moderaterna"] = float(data["data"][0]["values"][0])
                parties["Sverigedemokraterna"] = float(data["data"][7]["values"][0])
                parties["Övriga"] = float(data["data"][8]["values"][0])
                logos = {}
                logos["Moderaterna"] = ["111", "19", "(M) "]
                logos["Centerpartiet"] = ["231", "29", "(C) "]
                logos["Liberalerna"] = ["26",  "231","(L) "]
                logos["Kristdemokraterna"] = ["231", "26", "(KD)"]
                logos["Miljöpartiet"] = ["184", "34", "(MP)"]
                logos["Socialdemokraterna"] = ["231", "160","(S) "]
                logos["Vänsterpartiet"] = ["231", "124","(V) "]
                logos["Sverigedemokraterna"] = ["33",  "184","(SD)"]
                logos["Övriga"] = ["231", 239,"(*) "]
                descr = {}
                descr["Moderaterna"] = "Moderate Party [Liberal conservatism]"
                descr["Centerpartiet"] = "Centre Party [Liberalism, Agrarianism]"
                descr["Liberalerna"] = "Liberals [Liberalism, Social liberalism]"
                descr["Kristdemokraterna"] = "Christian Democrats [Christian democracy]"
                descr["Miljöpartiet"] = "Green Party [Green politics]"
                descr["Socialdemokraterna"] = "Swedish Social Democratic Party [Social democracy]"
                descr["Vänsterpartiet"] = "Left Party [Socialism, Feminist politics]"
                descr["Sverigedemokraterna"] = "Sweden Democrats [Social conservatism, Nationalism]"
                descr["Övriga"] = "Others"
                name = [None] * 9
                votes = [None] * 9
                logo = [None] * 9
                descrp = [None] * 9
                i = 0
                for key, value in sorted(parties.items(), key = itemgetter(1), reverse = True):
                    name[i] = key
                    votes[i] = value
                    logo[i] = logos[key]
                    descrp[i] = descr[key]
                    i = i + 1
                politicsStr = p
                politicsStr += "2018 Election Results for the " + placeData['municipality'] + " Municipal Assembly:                    \n"
                eights = [u"\u258F", u"\u258E", u"\u258D", u"\u258C", u"\u258B", u"\u258A", u"\u2589", u"\u2588", ""]
                for x in range(0,9):
                    fg = logo[x][0]
                    bg = logo[x][1]
                    barCol = co(bg, "233")
                    logod = f"{co(fg, bg)}{logo[x][2]}"
                    bar = (u"\u2588" * int(votes[x] // 1)) + eights[(floor((votes[x] % 1) / 0.125) ) -1]
                    politicsStr += f"\n{logod}{p}  {barCol}{bar}{p}{o} {str(votes[x])}% {p}{descrp[x]}\n"
                politicsStr += f"\n\nGrossly generalized reference scale of the Swedish political spectrum:\n\n" \
                             + f"{co(184,124)}  ☭  Far-left  {p} "
                spectrum = ["(V)", "(S)", "(MP)", "(C)", "(KD)", "(L)", "(M)", "(SD)"]
                for i in range(0,8):
                    for c in logo:
                        if spectrum[i] in c[2]:
                            politicsStr += f"{ co(c[0],c[1])}{c[2].replace(' ','')}{p} "
                politicsStr += f"{co(184,19)}  卐 Far-right  {p}"
            if ease.ex:
                draw.drawFrame()
                mv(36,0)
                politicsStr = o + "Something went wrong fetching data from statistikdatabasen.scb.se\n\n"+p
        return politicsStr

class CitizenSurvey:
    def __init__(self):
        self.update()
    def update(self):
        self.text = self.getCitizenSurvey()
    def getCitizenSurvey(self):
        with Loader("Getting citizen survey"):
            year = 2019
            noResult = True
            while (noResult) and (year > 2009):
                citizenSurveyPayload = {"query":[{"code":"Region","selection":{"filter":"item","values":[placeData['muniCode']]}},
                    {"code":"Kon","selection":{"filter":"item","values":["1+2"]}},
                    {"code": "MedborNRI","selection": {"filter": "item","values": [
                    "Fr A8:1", "Fr A9:1", "Fr A7:1", "Fr A7:2", "Fr A7:3", "Fr A1:1", "Fr A2:1",
                    "Fr A2:2", "Fr A3:1", "Fr A4:1", "Fr A4:2", "Fr A4:3", "Fr A5:1", "Fr A5:2",
                    "Fr A5:3", "Fr A6:1", "Fr A6:2", "Fr A6:3", "Fr A6:4", "Fr A6:5" ]}},
                    {"code":"ContentsCode","selection":{"filter":"item","values":["000000WO"]}},
                    {"code":"Tid","selection":{"filter":"item","values":[str(year)]}}],"response":{"format":"json"}}
                with ease(2, "API response error"):
                    resp = post("http://api.scb.se/OV0104/v1/doris/sv/ssd/START/ME/ME0003/MedborgarenA",data=dumps(citizenSurveyPayload))
                    # The response data is preceded with a BOM signature, so it can't be used as a dict directly (we have to shave it off first)
                    data = resp.text.encode().decode("utf-8-sig")
                with ease(2, "Error evaluating survey data"):
                    data = eval(data)
                if str( data["data"][0]["values"][0] ) == "..":
                    year=year-1
                else:
                    noResult = False
            surveyText = f"{o}The citizens of " + placeData["municipality"] + f" answered the following questions about their municipality in {year}:{y}\n\n"
            questions = [
                "How pleased are you in general with the municipality as a place to live in?",
                "Would you recommend your friends to move here?",
                "What do you think of...\n...the possibilities to get employment within a reasonable distance?",
                "...the availability of university or college educations within a reasonable distance?",
                "...the availability of other educations within a reasonable distance?",
                "...the possibilities to find good housing?",
                "...the availability of bicycle lanes and walkways?",
                "...the possibilities to use public transport for travelling?",
                "...the availability of long distance travel connections?",
                "...the availability of grocery stores within a reasonable distance?",
                "...the availability of other stores and services within a reasonable distance?",
                "...the availability of coffee shops/bars/restaurants within a reasonable distance?",
                "...the availability of recreation areas, parks and nature?",
                "...the possibilities to exercise hobbies such as sports, culture, outdoor life, club activities?",
                "...the availability of sports events?",
                "...the availability of culture events?",
                "...the availability of entertainment?",
                "How safe do you feel outdoors during evenings and nights?",
                "How safe do you feel against threats, muggings and assault?",
                "How safe do you feel against home invasions?"
            ]
            with ease(2, "Error parsing survey data"):
                x = 0
                for question in questions:
                    surveyText += question + f" {o}" + str(data["data"][x]["values"][0]) + f"/10{y}\n"
                    x=x+1
                pass
            if ease.ex:
                surveyText = "N/A"
            if noResult:
                surveyText = f"{placeData['municipality']} municipality has chosen not to use this standard citizen survey.\n" \
                           + f"There might be a local variant, try searching for " \
                           + f"\"{placeData['municipality']}s kommun medborgarundersökning\" on the web."
        return surveyText

class Dating:
    def __init__(self):
        pass
    def date(self):
        global i
        global ages
        gender = ""
        mv(35,0)
        print(f"{p}To give you a fair picture of your dating chances in {placeData['municipality']}, " +
              f"you are requested to answer a few questions:\n\n" +
              f"Do you want to date a {o}M{y}an, {o}W{y}oman or {o}E{y}ither?")
        genderAnsw = menu.getch()
        if genderAnsw.lower() == "m": gender = ["1"]
        elif genderAnsw.lower() == "w": gender = ["2"]
        elif genderAnsw.lower() == "e": gender = ["1", "2"]
        else: gender = ["1", "2"]
        draw.drawFrame()
        mv(35,0)
        print(f"What ages are you looking for? ({o}Y{p}es or {o}N{p}o)\n")
        agesQuery = []
        ages = ["15-19","20-24","25-29","30-34","35-39","40-44","45-49","50-54","55-59","60-64","65-69",\
                "70-74","75-79","80-84","85-89","90-94","95-99","100+"]
        for item in range(len(ages)):
            print(ages[item])
        for i in range(1,19):
            mv(i+36,7)
            print(f"{b}{y}<-----", end="")
            stdout.flush()
            rightAgeOrNot = menu.getch()
            if rightAgeOrNot.lower() == "y":
                mv(i+36,7)
                print(f"{b}{g}<-----", end=alloff)
                agesQuery.append(ages[i-1])
            else:
                mv(i+36,7)
                print(f"{b}{r}<-----", end=alloff)
        payload = {"query":[{"code":"Region","selection":{"filter":"vs:RegionKommun07","values":[placeData['muniCode']]}},\
                {"code":"Civilstand","selection":{"filter":"item","values":["OG","G","SK","ÄNKL"]}},{"code":"Alder",\
                "selection":{"filter":"agg:Ålder5år","values":agesQuery}},{"code":"Kon","selection":{"filter":"item",\
                "values":gender}},{"code":"ContentsCode","selection":{"filter":"item","values":["BE0101N1"]}},\
                {"code":"Tid","selection":{"filter":"item","values":["2017"]}}],"response":{"format":"json"}}
        draw.drawFrame()
        mv(35,0)
        with Loader("Getting marital status data"):
            with ease:
                resp = post("http://api.scb.se/OV0104/v1/doris/sv/ssd/START/BE/BE0101/BE0101A/BefolkningNy",data=dumps(payload))
                # The response data is preceded with a BOM signature, so it can't be used as a dict directly. We get rid of it by encoding it
                # and then decoding it again:
                data = resp.text.encode().decode("utf-8-sig")
                data = eval(data)
        with ease:
            mv(35,0)
            agesTot = len(agesQuery)
            agesTot *= len(gender)
            single = 0
            married = 0
            divorced = 0
            widow = 0
            theBar = ""
            for i in range(agesTot):
                single += int(data["data"][i]["values"][0])
            for i in range(agesTot, agesTot*2):
                married += int(data["data"][i]["values"][0])
            for i in range(agesTot*2, agesTot*3):
                divorced += int(data["data"][i]["values"][0])
            for i in range(agesTot*3, agesTot*4):
                widow += int(data["data"][i]["values"][0])
            percentSingle = ((single+divorced+widow)/(married+single+divorced+widow))*100
            print(f"Number of singles in {placeData['municipality']} out of the {single+married+divorced+widow} " \
                  f"people matching your target group:")
            mv(55,0)
            for i in range(1,101):
                mv(55,0)
                if i <= percentSingle:
                    currPercent = i
                    sleep(0.02 + currPercent/1200)
                    theBar = theBar + rb + "\u2665"
                else:
                    theBar = theBar + nb + gb + "\u2665" + fYel
                loveBar = y + b + "[" + b + theBar + (nb + gb + "\u2665" * (100-i)) + y + b + "]"
                mv(37,0)
                print(f"{b}{y}{currPercent:02d}% {nb}{loveBar}{b}")
            mv(39,0)
            print(f"{b}{y}Married: {nb}{fRed}{str(married)}\n")
            print(f"{b}{y}Single: {nb}{g}{str(single)}\n")
            print(f"{b}{y}Divorced: {nb}{g}{str(divorced)}\n")
            print(f"{b}{y}Widows/Widowers: {nb}{g}{str(widow)}\n")
            print(alloff)
        if ease.ex:
            draw.drawFrame()
            mv(36,0)
            print(f"{o}A fatal error occured while calculating your dating odds. Don't take it personally?\n\n{alloff}")

class Search:
    def __init__(self):
        pass
    def type(self):
        mv(35,0)
        print(f"{p}Do you want to search for {o}T{y}own (Postort), " \
            + f"{o}M{y}unicipality (Kommun), {o}C{y}ounty (Län), {o}P{y}ostcode (Postnummer) or {o}L{y}ong+Lat?")
        searchType = menu.getch()
        if searchType.lower() == "t": self.searchKey("postalTown","Town/Postort")
        elif searchType.lower() == "m": self.searchKey("municipality","Municipality/Kommun")
        elif searchType.lower() == "c": self.searchKey("county","County/Län")
        elif searchType.lower() == "p": self.searchKey("postalCode","Post code/Postnummer")
        elif searchType.lower() == "l": self.searchLoLa()
        else: print("Cancelled.")

    def searchKey(self, key, text):
        global zoomLevel
        global placeData
        menu.section = ""
        term = input(f"{p}{text}: ")
        print("Searching")
        term = term.rstrip().replace(" ", "") if key == "postalCode" else term.rstrip()
        newPlaceData = bigData.findByKey(key, term)
        if newPlaceData:
            placeData = newPlaceData
            print(y+"Found "+o+placeData['postalTown']+", "+placeData['municipality']+", "+placeData['county']+y)
            zoomLevel = defaultZoomLevel
            map.update()
            wiki.update()
            economy.update()
            mapImg = Image(name="tmp/map.jpg", width=118)
            coatImg = Image(name="tmp/coat.png", width=32)
            menu.section = "g"
            draw.drawFrame()
            mv(35,0)
            print(fill(wiki.text, 110), y)
            menu.menu()
        else:
            draw.drawFrame()
            mv(35,0)
            print("Found nothing.")

    def searchLoLa(self):
        global placeData
        global zoomLevel
        menu.section = ""
        lat = input(f"{p}Latitude: ")
        lat = lat.rstrip()
        lon = input(f"{p}Longitude: ")
        lon = lon.rstrip()
        print("Searching")
        term = {'lat':float(lat), 'lon':float(lon)}
        newPlaceData = bigData.gibClosest(term)
        if newPlaceData:
            placeData = newPlaceData
            print(y+"Found "+o+placeData['postalTown']+", "+placeData['municipality']+", "+placeData['county']+y)
            zoomLevel = defaultZoomLevel
            map.update()
            wiki.update()
            economy.update()
            mapImg = Image(name="tmp/map.jpg", width=118)
            coatImg = Image(name="tmp/coat.png", width=32)
            menu.section = "g"
            draw.drawFrame()
            mv(35,0)
            print(fill(wiki.text, 110), y)
            menu.menu()
        else:
            draw.drawFrame()
            mv(35,0)
            print("Found nothing.")

# Install any missing modules:
VirtualEnvironment().install_dependencies()

# Import modules:
import xml.etree.ElementTree as ET
import os, termios, pickle
from tty import setraw
from datetime import datetime
from time import sleep
from math import cos, asin, sqrt, modf, floor
from random import randrange
from subprocess import check_output
from operator import itemgetter
from sys import stdout, stdin, exit
from textwrap import dedent, fill
from requests import Session, post
from json import loads, dumps
from googletrans import Translator
from textwrap import fill
from itertools import cycle
from shutil import get_terminal_size
from threading import Thread

zoomLevel = defaultZoomLevel

# Create objects:
ease = ContextManager()
bigData = GibData()
placeData = bigData.findByKey("postalCode", "11130")
economy = Economy()
draw = Draw()
wiki = Wikipedia()
map = Maps(300, 200, "tmp/map.jpg")
with Loader("Loading translator"):
    translator = Translator()
latestCrime = Crimes()
dating = Dating()
politics = Politics()
citizenSurvey = CitizenSurvey()
with Loader("Loading search"):
    search = Search()
with Loader("Loading menu"):
    menu = Menu()

with Loader("Converting images"):
    # Convert images to ANSI escape codes:
    mapImg = Image(name="tmp/map.jpg", width=118)
    coatImg = Image(name="tmp/coat.png", width=32)
    frameImg = Image(name="data/frame.png", width=25)
    menuImg = Image(name="data/meny.png", width=117)

# Draw screen and show Wikipedia entry:
draw.drawFrame()
print(mv(35,0) + fill(wiki.text, 110), p)
menu.section = "g"
menu.menu()
